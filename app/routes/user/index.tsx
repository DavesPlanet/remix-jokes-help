
import { Outlet, useLoaderData, LoaderFunction, Link, redirect, ActionFunction } from "remix";


import { useState } from "react";

import { useActionData, Form } from "remix";


export let action: ActionFunction = async ({ request }): Promise<Response | ActionData> => {
    console.log("ACTION!!!")
    return redirect('.');
};




export default function UserIndexRoute () {

  let actionData = useActionData();

    const page = { margin: "20px" };
    const [userNotes, setUserNotes] = useState(actionData?.fields?.userNotes ?? "Some notes");

    const inputStyle = { marginBottom: "10px", fontSize: "larger"};

    return (
      <div style={page}>
        <h1>User Information</h1>


        <form method="post">

        <input type="text" name="userNotes" value={userNotes} onChange={setUserNotes}></input>
          <br/>

          <button type="submit" className="button">
              Save Changes
          </button>
        </form>
        </div>
    )

}